﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using sisvenda.Model;

public partial class sisvendasV2Context : DbContext
    {
        public sisvendasV2Context (DbContextOptions<sisvendasV2Context> options)
            : base(options)
        {
       
        // this.Configuration.LazyLoadingEnabled = false;
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.Entity<sisvendasV2.pedidoItem>()
        //     .HasKey(c => new { c.codigoPedido, c.codigoPedidoitem });
        // .HasKey(c => new {  c.codigoPedidoitem });
        modelBuilder.Entity<sisvenda.pedidoItem>(entity =>
       {
           entity.HasKey(e => new { e.codigoPedido, e.codigoPedidoitem });

           entity.Property(e => e.codigoPedidoitem).ValueGeneratedOnAdd();

         //  entity.Property(e => e.dataPedido).HasColumnType("datetime");

       //    entity.Property(e => e.valoritem).("decimal(18, 2)");

           //entity.HasOne(d => d.codigoPedidoNavigation)
           //    .WithMany(p => p.pedidoItem)
           //    .HasForeignKey(d => d.codigoPedido)
           //    .OnDelete(DeleteBehavior.ClientSetNull)
           //    .HasConstraintName("FK_pedidoItem_pedido");

           //entity.HasOne(d => d.codigoProdutoNavigation)
           //    .WithMany(p => p.pedidoItem)
           //    .HasForeignKey(d => d.codigoProduto)
           //    .OnDelete(DeleteBehavior.ClientSetNull)
           //    .HasConstraintName("FK_pedidoItem_produtos");
       });

        modelBuilder.Entity<sisvenda.Pedido>(entity =>
                {
                    entity.HasKey(e => e.codigoPedido);
                });
        //modelBuilder.Entity<sisvenda.Produtos>(entity =>
        //{
        //    entity.HasKey(e => e.codigoProduto);

        //});
        modelBuilder.Entity<Produtos>(entity =>
        {
            entity.HasKey(e => e.codigoProduto);
           // entity.HasOne(p => p.Colecao);
                //.WithMany(b => b.produtos)
                //.HasForeignKey(p => p.codigocolecao);
            //    .OnDelete(DeleteBehavior.ClientSetNull)
            //    .HasConstraintName("FK_pedidoItem_pedido");
        });
          //.HasOne(p => p.colecao)
          //.WithMany(b => b.produtos)
          //.HasForeignKey(p => p.codigocolecao);
    }
    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //{
    //    optionsBuilder
    //         .UseLazyLoadingProxies();

    //}

    public DbSet<sisvenda.Usuario> Usuario { get; set; }
    public DbSet<Produtos> produtos { get; set; }
    public DbSet<sisvenda.Colecao> Colecao { get; set; }
    public DbSet<sisvenda.Cliente> Cliente { get; set; }    

    public DbSet<sisvenda.Tamanho> Tamanho { get; set; }

    public DbSet<sisvenda.Pedido> Pedido { get; set; }
    public DbSet<sisvenda.pedidoItem> PedidoItem { get; set; }
    public DbSet<sisvenda.formapagamento> Formapagamento { get; set; }

}
