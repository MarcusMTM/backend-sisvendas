﻿using Microsoft.EntityFrameworkCore;
using sisvenda.Model;
using System;
using System.Collections.Generic;

using System.Threading.Tasks;
using sisvenda;

namespace sisvenda.Data
{
    public class sisdbcontext : DbContext
    {
        public sisdbcontext(DbContextOptions<sisdbcontext> options)
         : base(options)
        {

        }
        public DbSet<produto> produto { get; set; }
        public DbSet<tam> tam { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<produto>()
          .HasOne<tam>(s => s.tamanho)
          //  .WithMany(g => g.Produtos)
          .WithMany()
          .HasForeignKey(s => s.codigoTamanho);
            //modelBuilder.Entity<tam>()
            // // .HasMany<Produtos>(g => g.Produtos)
            // .HasMany<produto>()
            //   .WithOne(s => s.tamanho)
            //   .HasForeignKey(s => s.codigoTamanho);
        }
        public DbSet<Produtos> Produtos { get; set; }
    }
   
}
