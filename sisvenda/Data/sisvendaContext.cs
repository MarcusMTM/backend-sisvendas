﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sisvenda;
using sisvenda.Model;

public class sisvendaContext : DbContext
    {
        public sisvendaContext (DbContextOptions<sisvendaContext> options)
            : base(options)
        {
     
    }
    public DbSet<sisvenda.Usuario> Usuario { get; set; }
    public DbSet<Produtos> produtos { get; set; }
    public DbSet<sisvenda.Colecao> Colecao { get; set; }
    public DbSet<sisvenda.Cliente> Cliente { get; set; }

    public DbSet<sisvenda.Tamanho> Tamanho { get; set; }

    public DbSet<sisvenda.Pedido> Pedido { get; set; }
    public DbSet<sisvenda.pedidoItem> PedidoItem { get; set; }
    public DbSet<sisvenda.formapagamento> Formapagamento { get; set; }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {


        modelBuilder.Entity<sisvenda.pedidoItem>(b => 
        {
            b.HasKey(e => e.codigoPedidoitem);
            b.HasOne(p => p.Pedido)
                .WithMany()
                .HasForeignKey(p => p.codigoPedido);
            b.HasOne(p => p.produto)
               .WithMany()
               .HasForeignKey(p => p.codigoProduto);
            //b.HasOne("VendasSis.Pedido", "Pedido")
            //    .WithMany()
            //    .HasForeignKey("codigoPedido")
            //    .OnDelete(DeleteBehavior.Cascade)
            //    .IsRequired();

            //b.HasOne("VendasSis.Produtos", "produto")
            //    .WithMany()
            //    .HasForeignKey("produtocodigoProduto");
        });
        modelBuilder.Entity<Produtos>()
      .HasOne<Tamanho>(s => s.tamanho)
      //  .WithMany(g => g.Produtos)
      .WithMany()
      .HasForeignKey(s => s.codigoTamanho);
        modelBuilder.Entity<Tamanho>()
         // .HasMany<Produtos>(g => g.Produtos)
         .HasMany<Produtos>()
           .WithOne(s => s.tamanho)
           .HasForeignKey(s => s.codigoTamanho);
        modelBuilder.Entity<Colecao>()
        // .HasMany<Produtos>(g => g.Produtos)
        .HasMany<Produtos>()
          .WithOne(s => s.Colecao)
          .HasForeignKey(s => s.codigoColecao);

        modelBuilder.Entity<sisvenda.Pedido>(entity =>
        {
            entity.HasKey(e => e.codigoPedido);
        });
        modelBuilder.Entity<sisvenda.Tamanho>(entity =>
        {
            entity.HasKey(e => e.codigotamanho);
        });
        modelBuilder.Entity<sisvenda.formapagamento>(entity =>
        {
            entity.HasKey(e => e.codigoFormaPagamento);
        });

        base.OnModelCreating(modelBuilder);


    }
   
}
