﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sisvenda;
using sisvenda.Interface;

namespace sisvenda.Controllers
{
   // [EnableCors("CorsPolicy")]
    //[EnableCors("AllowOrigin")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ColecaosController : ControllerBase
    {
        private readonly sisvendaContext _context;
        private IGenericRepository<Colecao> repository = null;

        private readonly Igeneric<Colecao> _IRepositorio;

        public ColecaosController(sisvendaContext context)
        {
            _context = context;
        }

        // GET: api/Colecaos
        [HttpGet]

        public IEnumerable<Colecao> GetColecaos()
        {

            try
            {
                var teste = _context.Colecao.AsEnumerable();
                return teste;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      //  [EnableCors]
        // GET: api/Colecaos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Colecao>> GetColecao(int id)
        {
            var colecao = await _context.Colecao.FindAsync(id);

            if (colecao == null)
            {
                return NotFound();
            }

            return colecao;
        }
        //[EnableCors]
        // PUT: api/Colecaos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecao(int id, Colecao colecao)
        {
            if (id != colecao.codigoColecao)
            {
                return BadRequest();
            }

            _context.Entry(colecao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColecaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
       // [EnableCors]
        // POST: api/Colecaos
        [HttpPost]
        public async Task<ActionResult<Colecao>> PostColecao(Colecao colecao)
        {
            _context.Colecao.Add(colecao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColecao", new { id = colecao.codigoColecao }, colecao);
        }
        //[EnableCors]
        // DELETE: api/Colecaos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Colecao>> DeleteColecao(int id)
        {
            var colecao = await _context.Colecao.FindAsync(id);
            if (colecao == null)
            {
                return NotFound();
            }

            _context.Colecao.Remove(colecao);
            await _context.SaveChangesAsync();

            return colecao;
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecao.Any(e => e.codigoColecao == id);
        }
    }
}
