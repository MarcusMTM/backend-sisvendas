﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Sisvenda;
using sisvenda.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace sisvenda.Controllers
{
    //[EnableCors("CorsPolicy2")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class tamanhoesController : ControllerBase
    {
        private readonly sisvendaContext _context;
        private IGenericRepository<Tamanho> repository = null;

        private readonly Igeneric<Tamanho> _IRepositorio;

        public tamanhoesController(sisvendaContext context)
        {
            _context = context;
        }

        // GET: api/tamanhoes
        [HttpGet]
     
        public IEnumerable<Tamanho> Gettamanho()
        {

            try
            {
                var teste = _context.Tamanho.ToList();
                return teste;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // GET: api/tamanhoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tamanho>> Gettamanho(int id)
        {
            var tamanho = await _context.Tamanho.FindAsync(id);

            if (tamanho == null)
            {
                return NotFound();
            }

            return tamanho;
        }
     
        // PUT: api/tamanhoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Puttamanho(int id, Tamanho tamanho)
        {
            if (id != tamanho.codigotamanho)
            {
                return BadRequest();
            }

            _context.Entry(tamanho).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tamanhoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        //[EnableCors]
        // POST: api/tamanhoes
        [HttpPost]
        public async Task<ActionResult<Tamanho>> Posttamanho(Tamanho tamanho)
        {
            _context.Tamanho.Add(tamanho);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Gettamanho", new { id = tamanho.codigotamanho }, tamanho);
        }

        // DELETE: api/tamanhoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Tamanho>> Deletetamanho(int id)
        {
            var tamanho = await _context.Tamanho.FindAsync(id);
            if (tamanho == null)
            {
                return NotFound();
            }

            _context.Tamanho.Remove(tamanho);
            await _context.SaveChangesAsync();

            return tamanho;
        }

        private bool tamanhoExists(int id)
        {
            return _context.Tamanho.Any(e => e.codigotamanho == id);
        }
    }
}
