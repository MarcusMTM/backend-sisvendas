﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using sisvenda.Configuration;
using sisvenda.Repository;
using sisvenda.Services;
using static sisvenda.Model.ViewModels;

namespace sisvenda.Controllers
{
    
    [ApiController]
    //[Route("v1/account")]
    public class AuthController : ControllerBase
    {
        private readonly sisvendaContext _context;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly Settings _appSettings;
        public AuthController(sisvendaContext context)
        {
            _context = context;
        }
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Authenticate(Usuario loginUser)
        {
            //refazer o login,pegar o que esta cadastrado no banco e ajustar as noti
            // Recupera o usuário
            var user = _context.Usuario.Where(a => a.login.Equals(loginUser.login) && a.senha.Equals(loginUser.senha)).FirstOrDefault();
        

               // var user = UserRepository.Get(loginUser.Email, loginUser.Password);

            // Verifica se o usuário existe
            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            // Gera o Token
            //return (await GerarJwt(model));
            // var token = GerarJwt(model.Username);
            var token = TokenService.GenerateToken(user);

            // Oculta a senha
            user.senha = "";

            // retorna os dados
            //return new
            //{
            //    user = user,
            //    token = token
            //};
           // return await GerarJwt(user);
            var response = new LoginResponseViewModel
            {
                AccessToken = token,
                ExpiresIn = TimeSpan.FromHours(1).TotalSeconds,
                UserToken = new UserTokenViewModel
                {
                    Id =user.codigoUsuario,
                    Email = user.email,
                    Nomeusuario=user.nomeusuario,


                }
            };

            return response;
        }
        private async Task<LoginResponseViewModel> GerarJwt(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            var claims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(DateTime.UtcNow).ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64));
            //foreach (var userRole in userRoles)
            //{
            //    claims.Add(new Claim("role", userRole));
            //}

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _appSettings.Emissor,
                Audience = _appSettings.ValidorEM,
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(_appSettings.Expericao),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            var encodedToken = tokenHandler.WriteToken(token);

            var response = new LoginResponseViewModel
            {
                AccessToken = encodedToken,
                ExpiresIn = TimeSpan.FromHours(_appSettings.Expericao).TotalSeconds,
                UserToken = new UserTokenViewModel
                {
                   // Id = user.Id,
                    Email = user.Email,
                    Claims = claims.Select(c => new ClaimViewModel { Type = c.Type, Value = c.Value })
                }
            };

            return response;
        }

        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }


}