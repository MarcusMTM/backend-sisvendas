﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sisvenda;

namespace sisvenda.Controllers
{
   // [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class formapagamentoesController : ControllerBase
    {
        private readonly sisvendaContext _context;

        public formapagamentoesController(sisvendaContext context)
        {
            _context = context;
        }

        // GET: api/formapagamentoes
        [HttpGet]
      
        public IEnumerable<formapagamento> GetColecaos()
        {

            try
            {
                var teste = _context.Formapagamento.AsEnumerable();
                return teste;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // GET: api/formapagamentoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<formapagamento>> Getformapagamento(int id)
        {
            var formapagamento = await _context.Formapagamento.FindAsync(id);

            if (formapagamento == null)
            {
                return NotFound();
            }

            return formapagamento;
        }

        // PUT: api/formapagamentoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putformapagamento(int id, formapagamento formapagamento)
        {
            if (id != formapagamento.codigoFormaPagamento)
            {
                return BadRequest();
            }

            _context.Entry(formapagamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!formapagamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/formapagamentoes
        [HttpPost]
        public async Task<ActionResult<formapagamento>> Postformapagamento(formapagamento formapagamento)
        {
            _context.Formapagamento.Add(formapagamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getformapagamento", new { id = formapagamento.codigoFormaPagamento }, formapagamento);
        }

        // DELETE: api/formapagamentoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<formapagamento>> Deleteformapagamento(int id)
        {
            var formapagamento = await _context.Formapagamento.FindAsync(id);
            if (formapagamento == null)
            {
                return NotFound();
            }

            _context.Formapagamento.Remove(formapagamento);
            await _context.SaveChangesAsync();

            return formapagamento;
        }

        private bool formapagamentoExists(int id)
        {
            return _context.Formapagamento.Any(e => e.codigoFormaPagamento == id);
        }
    }
}
