﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sisvenda;
using sisvenda.Data;
using sisvenda.Model;

namespace sisvenda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Produtos1Controller : ControllerBase
    {
        private readonly sisdbcontext _context;

        public Produtos1Controller(sisdbcontext context)
        {
            _context = context;
        }

        // GET: api/Produtos1
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Produtos>>> GetProdutos()
        {
            try
            {
                //var teste = _context.produtos
                //                   .Include(i => i.Colecao)
                //                   .Include(i => i.tamanho).ToList();
                var teste = _context.Produtos
                     .Include(i => i.tamanho)
                                  .ToList();
                //var teste = _context.produtos.AsEnumerable();
                return Ok(teste);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Produtos1/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Produtos>> GetProdutos(int id)
        {
            var produtos = await _context.Produtos.FindAsync(id);

            if (produtos == null)
            {
                return NotFound();
            }

            return produtos;
        }

        // PUT: api/Produtos1/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProdutos(int id, Produtos produtos)
        {
            if (id != produtos.codigoProduto)
            {
                return BadRequest();
            }

            _context.Entry(produtos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Produtos1
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Produtos>> PostProdutos(Produtos produtos)
        {
            _context.Produtos.Add(produtos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProdutos", new { id = produtos.codigoProduto }, produtos);
        }

        // DELETE: api/Produtos1/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Produtos>> DeleteProdutos(int id)
        {
            var produtos = await _context.Produtos.FindAsync(id);
            if (produtos == null)
            {
                return NotFound();
            }

            _context.Produtos.Remove(produtos);
            await _context.SaveChangesAsync();

            return produtos;
        }

        private bool ProdutosExists(int id)
        {
            return _context.Produtos.Any(e => e.codigoProduto == id);
        }
    }
}
