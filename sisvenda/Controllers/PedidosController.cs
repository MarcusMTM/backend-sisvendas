﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sisvenda;
using sisvenda.Model;

namespace sisvenda.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PedidosController : ControllerBase
    {
        private readonly sisvendaContext _context;

        public PedidosController(sisvendaContext context)
        {
            _context = context;
        }

        // GET: api/Pedidos
        [HttpGet]
        public IEnumerable<Pedido> GetPedido()
        {
            try
            {
                var teste = _context.Pedido.Include(i => i.Cliente)                                 
                                    .Include(i => i.Formapagamento).ToList();
                return teste;
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        // [HttpGet("{id}")]
      
        [HttpGet("/PedidoResumo/{id}")]
        public async Task<ActionResult<Pedido>> GetPedidoResumo(int id)
        {
            var item = await _context.Pedido
                .Include(o => o.Cliente)
                .Include(i => i.Formapagamento).FirstOrDefaultAsync(i => i.codigoPedido == id);

            if (item == default(Pedido))
                return NotFound();
            //var Pedido = await _context.Pedido.FindAsync(id);

            //if (Pedido == null)
            //{
            //    return NotFound();
            //}

            return item;
        }
        // GET: api/Pedidos/5
        [HttpGet("{id}")]
        public async  Task<ActionResult<IEnumerable<Pedido>>> GetPedido(int id)
        {
            var result = await _context.Pedido.Include(i => i.Cliente)
                                    .Include(i => i.Formapagamento).ToListAsync();
            var pedido = result.Where(c => c.codigoPedido == id).ToList();
            // var pedido = await _context.Pedido.FindAsync(id);

            if (pedido == null)
            {
                return NotFound();
            }

            return pedido;
        }

        // PUT: api/Pedidos/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPedido(int id, Pedido pedido)
        {
            if (id != pedido.codigoPedido)
            {
                return BadRequest();
            }

            _context.Entry(pedido).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PedidoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
      //  [EnableCors]
        // POST: api/Pedidos
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Pedido>> PostPedido(Pedido pedido)
        {
            _context.Pedido.Add(pedido);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPedido", new { id = pedido.codigoPedido }, pedido);
        }

        // DELETE: api/Pedidos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Pedido>> DeletePedido(int id)
        {
            var pedido = await _context.Pedido.FindAsync(id);
            if (pedido == null)
            {
                return NotFound();
            }

            _context.Pedido.Remove(pedido);
            await _context.SaveChangesAsync();

            return pedido;
        }
        //[EnableCors]
        [HttpPost("/cadastrartodos")]
        public async Task<ActionResult<Pedido>> cadastrarPedidoItens(pedidoPedidoitem forms)
        {
            pedidoItem pedidoitem = new pedidoItem();
            Pedido pedido = new Pedido();
            pedido.codigoCliente = forms.codigoCliente;
            pedido.formapagamentoCodigo = forms.formapagamentoCodigo;
            pedido.dataPedido = forms.dataPedido;
            pedido.valor = forms.valor;
            pedido.status = forms.status;
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (forms.codigoPedido == 0)
                    {
                        _context.Pedido.Add(pedido);
                        await _context.SaveChangesAsync();
                        // _context.SaveChanges();

                        pedidoitem.codigoPedido = pedido.codigoPedido;
                    }
                    else
                    {
                        pedidoitem.codigoPedido = forms.codigoPedido;
                    }


                    pedidoitem.codigoProduto = forms.codigoProduto;
                    pedidoitem.dataPedido = forms.dataPedido;
                    pedidoitem.quantidadeItem = forms.quantidadeItem;
                    pedidoitem.status = forms.status;
                    pedidoitem.valoritem = forms.valoritem;

                    // _context.Pedido.Add(pedido);
                    _context.PedidoItem.Add(pedidoitem);
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    //return Ok(pedido);
                    return CreatedAtAction("GetPedido", new { id = pedido.codigoPedido }, pedido);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception();
                }
            }
        }
        private bool PedidoExists(int id)
        {
            return _context.Pedido.Any(e => e.codigoPedido == id);
        }

        [HttpGet("/resultadoProdPed/{id}")]
        public IActionResult resultadoProdPed(int id)
        {
            
            var query = from prod in _context.Set<Produtos>()
                        join p in _context.Set<pedidoItem>().
                        Where(i=>i.codigoPedido == id) 
                        on prod.codigoProduto equals p.codigoProduto
                        select new
                        {
                            p.codigoPedidoitem,
                            p.codigoPedido,
                            prod.descricao,
                            prod.codigoProduto,
                            prod.valor,
                            p.quantidadeItem
                        };

            var result = query.ToList();
             if (result == null){
                return NotFound();
            }

            return Ok(result);
        }
    }
}
