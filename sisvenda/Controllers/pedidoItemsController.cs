﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace sisvenda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class pedidoItemsController : ControllerBase
    {
        private readonly sisvendaContext _context;

        public pedidoItemsController(sisvendaContext context)
        {
            _context = context;
        }

        // GET: api/pedidoItems
        [HttpGet]
        public IEnumerable<pedidoItem> GetPedidoItem()
        {
           // return await _context.PedidoItem.ToListAsync();

            try
            {
                var teste = _context.PedidoItem
                                    .Include(i => i.produto)
                                
                                    .ToList();
                //var teste = _context.produtos.ToList();

                return teste;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/pedidoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<pedidoItem>>> GetpedidoItem(int id)
        {
            var result = await _context.PedidoItem.Include(i => i.produto)
                                   .Include(i => i.Pedido).ToListAsync();
            var pedidoItem = result.Where(c => c.codigoPedido == id).ToList();
            // var pedido = await _context.Pedido.FindAsync(id);

            if (pedidoItem == null)
            {
                return NotFound();
            }

            return pedidoItem;

     
        }
        [HttpGet("/PedidoItemResumo/{id}")]
        public async Task<ActionResult<pedidoItem>> GetPedidoItemResumo(int id)
        {
            var item = await _context.PedidoItem
                .Include(o => o.produto)
                .Include(i => i.Pedido).FirstOrDefaultAsync(i => i.codigoPedido == id);

            if (item == default(pedidoItem))
                return NotFound();
            //var Pedido = await _context.Pedido.FindAsync(id);

            //if (Pedido == null)
            //{
            //    return NotFound();
            //}

            return item;
        }

        // PUT: api/pedidoItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutpedidoItem(int id, pedidoItem pedidoItem)
        {
            if (id != pedidoItem.codigoPedido)
            {
                return BadRequest();
            }

            _context.Entry(pedidoItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!pedidoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/pedidoItems
        [HttpPost]
        public async Task<ActionResult<pedidoItem>> PostpedidoItem(pedidoItem pedidoitem)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.PedidoItem.Add(pedidoitem);
                    await _context.SaveChangesAsync();
                    transaction.Commit();
                    return CreatedAtAction("GetpedidoItem", new { id = pedidoitem.codigoPedidoitem }, pedidoitem);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception();
                }
            }

                return CreatedAtAction("GetpedidoItem", new { id = pedidoitem.codigoPedidoitem }, pedidoitem);
        }

       

        // DELETE: api/pedidoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<pedidoItem>> DeletepedidoItem(int id)
        {
            var pedidoItem = await _context.PedidoItem.FindAsync(id);
            if (pedidoItem == null)
            {
                return NotFound();
            }

            _context.PedidoItem.Remove(pedidoItem);
            await _context.SaveChangesAsync();

            return pedidoItem;
        }

        private bool pedidoItemExists(int id)
        {
            return _context.PedidoItem.Any(e => e.codigoPedido == id);
        }
    }
}
