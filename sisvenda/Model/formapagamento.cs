﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class formapagamento
    {
        [Key]
        public int codigoFormaPagamento { get; set; }
        public string descricao { get; set; }
        public Boolean status { get; set; }
        //public ICollection<Pedido> Pedido { get; set; }
    }
}
