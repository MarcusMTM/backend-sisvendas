﻿using sisvenda.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sisvenda
{
    public partial class pedidoItem
    {
        // [Key, Column(Order = 1)]
        [ForeignKey("Pedido")]
        public int codigoPedido { get; set; }
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codigoPedidoitem { get; set; }
            

       // public Pedido pedido { get; set; }

        [ForeignKey("Produtos")]
        public int codigoProduto { get; set; }

        public Produtos produto { get; set; }

        public decimal valoritem { get; set; }

        public DateTime dataPedido { get; set; }
        public int quantidadeItem { get; set; }

        public Boolean status { get; set; }
        public  Pedido Pedido { get; set; }
      
    }
}
