﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda.Model
{
    public class tam
    {
        [Key]
        public int codigotamanho { get; set; }
        public string descricao { get; set; }

        public ICollection<produto> produto { get; set; } 
    }
}
