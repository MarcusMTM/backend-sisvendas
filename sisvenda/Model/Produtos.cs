﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda.Model
{
    public  class Produtos
    {
      
        [Key]
        public int codigoProduto { get; set; }
        public string descricao  { get; set; }

        [ForeignKey("codigoColecao")]
        public int codigoColecao { get; set; }

        public Colecao Colecao { get; set; }

        [ForeignKey("codigoTamanho")]
        public int codigoTamanho { get; set; }
        // public virtual tamanho tamanho { get; set; }
        public Tamanho tamanho { get; set; }
        //  public tamanho tamanho { get; set; }
        

        public decimal valor { get; set; }
}
}
