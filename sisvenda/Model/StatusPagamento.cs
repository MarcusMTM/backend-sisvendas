﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class StatusPagamento
    {
        [Key]
        public int codigoStatusPagamento { get; set; }
        public string descricao { get; set; }

    }
}
