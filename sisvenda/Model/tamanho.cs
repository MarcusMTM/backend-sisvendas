﻿using sisvenda.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class Tamanho
    {
        [Key]
        public int codigotamanho { get; set; }
        public string descricao { get; set; }

       //public ICollection<Produtos> Produtos { get; set; }
    }
}
