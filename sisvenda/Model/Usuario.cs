﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class Usuario
    {
        [Key]
        public int codigoUsuario { get; set; }
        public string nomeusuario { get; set; }
        public string cpf { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
        public Boolean status { get; set; }
        public string email { get; set; }

    }
}
