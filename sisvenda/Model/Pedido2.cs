﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public partial class Pedido2
    {
        //public Pedido()
        //{
        //    pedidoItem = new HashSet<pedidoItem>();
        //}

        public int codigoPedido { get; set; }

        public DateTime dataPedido { get; set; }

        [ForeignKey("Formapagamento")]
        public int formapagamentoCodigo { get; set; }
        public formapagamento Formapagamento { get; set; }


        [ForeignKey("Cliente")]
        public int codigoCliente { get; set; }
        public Cliente Cliente { get; set; }

        public decimal valor { get; set; }

        public Boolean status { get; set; }


        public  pedidoItem pedidoItem { get; set; }
        //public virtual ICollection<pedidoItem> pedidoItem { get; set; }
    }
}
