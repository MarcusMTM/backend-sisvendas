﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda.Model
{
    public class pedidoproduto
    {
        public int codigoPedido { get; set; }
        public int codigoPedidoitem { get; set; }
        public int codigoProduto { get; set; }
        public decimal valor { get; set; }
        public string descricao { get; set; }


    }
}
