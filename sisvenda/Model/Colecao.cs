﻿using sisvenda.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class Colecao
    {
        [Key]
       public int codigoColecao { get; set; }

        public string descricao { get; set; }
        public DateTime dataValidade { get; set; }
      
         //public ICollection<Produtos> produtos { get; set; }

    }
}
