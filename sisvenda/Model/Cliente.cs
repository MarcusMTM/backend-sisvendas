﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda
{
    public class Cliente
    {
        [Key]
        public int codigoCliente { get; set; }
        public string Nome { get; set; }
        public DateTime dataNascimento { get; set; }
        public string instagram { get; set; }
        public string whatsapp { get; set; }
        public string email { get; set; }
        //public ICollection<Pedido> Pedido { get; set; }
    }
}
