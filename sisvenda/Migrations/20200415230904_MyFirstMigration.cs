﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sisvenda.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    codigoCliente = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: true),
                    dataNascimento = table.Column<DateTime>(nullable: false),
                    instagram = table.Column<string>(nullable: true),
                    whatsapp = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.codigoCliente);
                });

            migrationBuilder.CreateTable(
                name: "Colecao",
                columns: table => new
                {
                    codigoColecao = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    descricao = table.Column<string>(nullable: true),
                    dataValidade = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Colecao", x => x.codigoColecao);
                });

            migrationBuilder.CreateTable(
                name: "Formapagamento",
                columns: table => new
                {
                    codigoFormaPagamento = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    descricao = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Formapagamento", x => x.codigoFormaPagamento);
                });

            migrationBuilder.CreateTable(
                name: "Tamanho",
                columns: table => new
                {
                    codigotamanho = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tamanho", x => x.codigotamanho);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    codigoUsuario = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nomeusuario = table.Column<string>(nullable: true),
                    cpf = table.Column<string>(nullable: true),
                    login = table.Column<string>(nullable: true),
                    senha = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false),
                    email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.codigoUsuario);
                });

            migrationBuilder.CreateTable(
                name: "Pedido",
                columns: table => new
                {
                    codigoPedido = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dataPedido = table.Column<DateTime>(nullable: false),
                    formapagamentoCodigo = table.Column<int>(nullable: false),
                    codigoCliente = table.Column<int>(nullable: false),
                    valor = table.Column<decimal>(nullable: false),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pedido", x => x.codigoPedido);
                    table.ForeignKey(
                        name: "FK_Pedido_Cliente_codigoCliente",
                        column: x => x.codigoCliente,
                        principalTable: "Cliente",
                        principalColumn: "codigoCliente",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pedido_Formapagamento_formapagamentoCodigo",
                        column: x => x.formapagamentoCodigo,
                        principalTable: "Formapagamento",
                        principalColumn: "codigoFormaPagamento",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "produtos",
                columns: table => new
                {
                    codigoProduto = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    descricao = table.Column<string>(nullable: true),
                    codigoColecao = table.Column<int>(nullable: false),
                    codigoTamanho = table.Column<int>(nullable: false),
                    valor = table.Column<decimal>(nullable: false),
                    ColecaocodigoColecao = table.Column<int>(nullable: true),
                    tamanhocodigotamanho = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_produtos", x => x.codigoProduto);
                    table.ForeignKey(
                        name: "FK_produtos_Colecao_ColecaocodigoColecao",
                        column: x => x.ColecaocodigoColecao,
                        principalTable: "Colecao",
                        principalColumn: "codigoColecao",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_produtos_Tamanho_tamanhocodigotamanho",
                        column: x => x.tamanhocodigotamanho,
                        principalTable: "Tamanho",
                        principalColumn: "codigotamanho",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PedidoItem",
                columns: table => new
                {
                    codigoPedidoitem = table.Column<int>(nullable: false),
                    codigoPedido = table.Column<int>(nullable: false),
                    codigoProduto = table.Column<int>(nullable: false),
                    valoritem = table.Column<decimal>(nullable: false),
                    dataPedido = table.Column<DateTime>(nullable: false),
                    quantidadeItem = table.Column<int>(nullable: false),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedidoItem", x => x.codigoPedidoitem);
                    table.ForeignKey(
                        name: "FK_PedidoItem_Pedido_codigoPedido",
                        column: x => x.codigoPedido,
                        principalTable: "Pedido",
                        principalColumn: "codigoPedido",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PedidoItem_produtos_codigoProduto",
                        column: x => x.codigoProduto,
                        principalTable: "produtos",
                        principalColumn: "codigoProduto",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pedido_codigoCliente",
                table: "Pedido",
                column: "codigoCliente");

            migrationBuilder.CreateIndex(
                name: "IX_Pedido_formapagamentoCodigo",
                table: "Pedido",
                column: "formapagamentoCodigo");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoItem_codigoPedido",
                table: "PedidoItem",
                column: "codigoPedido");

            migrationBuilder.CreateIndex(
                name: "IX_PedidoItem_codigoProduto",
                table: "PedidoItem",
                column: "codigoProduto");

            migrationBuilder.CreateIndex(
                name: "IX_produtos_ColecaocodigoColecao",
                table: "produtos",
                column: "ColecaocodigoColecao");

            migrationBuilder.CreateIndex(
                name: "IX_produtos_tamanhocodigotamanho",
                table: "produtos",
                column: "tamanhocodigotamanho");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PedidoItem");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Pedido");

            migrationBuilder.DropTable(
                name: "produtos");

            migrationBuilder.DropTable(
                name: "Cliente");

            migrationBuilder.DropTable(
                name: "Formapagamento");

            migrationBuilder.DropTable(
                name: "Colecao");

            migrationBuilder.DropTable(
                name: "Tamanho");
        }
    }
}
