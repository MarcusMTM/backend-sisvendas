﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda.Interface
{
    interface Igeneric<T> where T : class
    {
        void Add(T item);
        IEnumerable<T> GetAll();
        T Find(long key);
        void Remove(long key);
        void Update(T item);
    }

}
