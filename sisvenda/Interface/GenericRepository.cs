﻿using Microsoft.EntityFrameworkCore;
using sisvenda.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sisvenda.Interface
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private sisvendasV2Context _context;
       // private EmployeeDBContext _context = null;
        private DbSet<T> table = null;
        
        public GenericRepository()
        {
          
            table = _context.Set<T>();
        }

        public GenericRepository(sisvendasV2Context _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return table.ToList();
        }

        public T GetById(object id)
        {
            return table.Find(id);
        }

        public void Insert(T obj)
        {
            table.Add(obj);
        }

        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }

        public void Delete(object id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
